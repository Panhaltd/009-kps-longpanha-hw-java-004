import java.util.ArrayList;

public class GenericList<T> {
    ArrayList<T> list = new ArrayList<>();
    void addItem(T obj) throws CustomException {
        if (obj==null){
            throw new CustomException("Null Value Has Inputted!");
        }else if (list.contains(obj)){
            throw new CustomException("Duplicate Values Have Inputted!");
        }else {
            list.add(obj);
        }
    }
    T get(int i){
        return list.get(i);
    }
}
